def perfometer_check_mk_fail2ban(row, check_command, perf_data):
	color = { 0: "#0f0", 1: "#ff2", 2: "#f22", 3: "#fa2" }[row["service_state"]]
	cfail = perf_data[0][1]
	cban = perf_data[1][1]
	tfail = perf_data[2][1]
	tban = perf_data[3][1]
	return "%s fails|%s bans" % (cfail, cban), perfometer_logarithmic(cban, 50, 2, color)

perfometers["check_mk-fail2ban"]  = perfometer_check_mk_fail2ban
