#!/usr/bin/python
# -*- encoding: utf-8; py-indent-offset: 4 -*-

# Rules for configuring parameters of check fail2ban

subgroup_networking =   _("Networking")

register_check_parameters(
	subgroup_applications,
	"fail2ban",
	_("Fail2Ban"),
	#Transform(
	Dictionary(
		help = _("This ruleset can be used to change the failure/ban warning and crit levels or to disable them."),
		elements = [
			("levels_fail",
				Tuple(
					title = _("Number of failures"),
					elements = [
						Integer(title = _("Warning at"), default_value = 50, min_value = 0 ),
						Integer(title = _("Critical at"), default_value = 100, min_value = 0 ),
						],
					help = _("You can adjust the number of failures before this service goes into warning/critical. Set to 0 to disable."),
					),
				), 
			("levels_ban",
				Tuple(
					title = _("Number of bans"),
					elements = [
						Integer(title = _("Warning at"), default_value = 25, min_value = 0 ),
						Integer(title = _("Critical at"), default_value = 50, min_value = 0 ),
						],
					help = _("You can adjust the number of bans before this service goes into warning/critical. Set to 0 to disable."),
					),
				),
			],
		optional_keys = False,
	),
	#	forth = lambda old: type(old) != dict and { "levels_fail" : old[0,2] } and { "levels_ban" : old[2,4] } or old,
	#	),
	TextAscii(
		title = _("Fail2ban jail"),
		help = _("Specify the name of the Fail2ban Jail, i.e. <tt>ssh</tt>"),
		allow_empty = False
	),
	'dict'
)
