<?php
# PNP4Nagios template for check_mk fail2ban check
# Author: Wouter de Geus <benv-check_mk@junerules.com>

setlocale(LC_ALL, "POSIX");
$desc = str_replace("_", " ", $servicedesc);

$opt[1]  = "--vertical-label 'Fails|Bans' --title '$hostname: Fail2Ban $desc' ";

# Color
$COLORS['totfail'] = '007bff';
$COLORS['totban']  = '33ffff';
$COLORS['curfail'] = '3384aa';
$COLORS['curban']  = 'aa0000';

$def[1] =  "DEF:cfail=$RRDFILE[1]:$DS[1]:AVERAGE " ;
$def[1] .= "DEF:cban=$RRDFILE[2]:$DS[2]:AVERAGE " ;
$def[1] .= "DEF:tfail=$RRDFILE[3]:$DS[3]:AVERAGE " ;
$def[1] .= "DEF:tban=$RRDFILE[4]:$DS[4]:AVERAGE " ;

# area's
# Skip the totals, they are too big and always rising => makes the graph uninteresting.
# $def[1] .= "AREA:tfail#".$COLORS['totfail'].":\"Total fail   \" " ;
# $def[1] .= "GPRINT:tfail:LAST:\"%6.1lf last\" " ;
# $def[1] .= "GPRINT:tfail:AVERAGE:\"%6.1lf avg\" " ;
# $def[1] .= "GPRINT:tfail:MAX:\"%6.1lf max\\n\" ";

# $def[1] .= "AREA:tban#".$COLORS['totban'].":\"Total ban    \" " ;
# $def[1] .= "GPRINT:tban:LAST:\"%6.1lf last\" " ;
# $def[1] .= "GPRINT:tban:AVERAGE:\"%6.1lf avg\" " ;
# $def[1] .= "GPRINT:tban:MAX:\"%6.1lf max\\n\" ";

$def[1] .= "AREA:cfail#".$COLORS['curfail'].":\"Current fail \" " ;
$def[1] .= "GPRINT:cfail:LAST:\"%6.1lf last\" " ;
$def[1] .= "GPRINT:cfail:AVERAGE:\"%6.1lf avg\" " ;
$def[1] .= "GPRINT:cfail:MAX:\"%6.1lf max\\n\" ";

$def[1] .= "LINE2:cban#".$COLORS['curban'].":\"Current ban  \" " ;
$def[1] .= "GPRINT:cban:LAST:\"%6.1lf last\" " ;
$def[1] .= "GPRINT:cban:AVERAGE:\"%6.1lf avg\" " ;
$def[1] .= "GPRINT:cban:MAX:\"%6.1lf max\\n\" ";

# Warning/crit levels
if ($WARN[1]) {
 $def[1] .= "HRULE:$WARN[1]#FFFF00:\"Warning level\: $WARN[1]\" ";
}
if ($CRIT[1]) {
 $def[1] .= "HRULE:$CRIT[1]#FF0000:\"Critical level\: $CRIT[1]\\n\" ";
}

$def[1] .= "GPRINT:tfail:LAST:\"Total fails %6.1lf \" " ;
$def[1] .= "GPRINT:tban:LAST:\"Total bans %6.1lf \\n\" ";

?>
